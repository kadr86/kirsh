$(document).on "page:load ready", -> 
 images_count = $('ul.prettyGallery').attr('data-item_count')
 
 if images_count > 5
 	navigation = 'bottom'
 	itemsPage = 5
 else 
 	 itemsPage = images_count - 1
 	 navigation = 'none'
 	 css = 'no-arrows'
 	 $('ul.prettyGallery').addClass css

 $('ul.prettyGallery').prettyGallery
  itemsPerPage : itemsPage, 
  navigation : navigation
$(document).on 'click', '.photo_mini', ->
	src = $(@).attr('src').replace('small','medium')
	$('#photo_big').attr('src',src)