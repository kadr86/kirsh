class Client < ActiveRecord::Base
	has_one :client_request

	validates :phone, :name, presence: true
end
