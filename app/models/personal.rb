class Personal < ActiveRecord::Base
	mount_uploader :avatar, AvatarUploader
	belongs_to :user_account
	has_many :client_requests
    has_many :sale_objects, dependent: :nullify 


	scope :rieltor, -> {where(position: 15)}

	validates :sename, :name, :status, :position, :agency, :begin_job, :mobile_phone, presence: true
end
