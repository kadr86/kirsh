# encoding: utf-8

class ClientRequestWithoutObject < ActiveRecord::Base
	belongs_to :personal

	validates  :phone, :name, presence: true

	after_save do
  	client = Smspilot.new "H69SXX2EXAP3E2TI92W03GTT955PQ9ZLFSQXSO51C1NH65K57TZIUDF88YTPK024"    
    result = client.send_sms(self.id, 'KIRSH', self.personal.mobile_phone.delete('-'), "#{self.name}, тел. #{self.phone}#{',' unless self.comment.blank?} #{self.comment}")
  end
end
