# encoding: utf-8

class Question < ActiveRecord::Base
	apply_simple_captcha :message => "Не правильно введен код с картинки"
	has_one :answer, dependent: :destroy 
	validates :name, :question, :email, presence: true

	scope :public, ->{where(is_public: 1)}

end
