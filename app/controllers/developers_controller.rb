class DevelopersController < ApplicationController
  def edit
  	@developer = Developer.find params[:id]
  end

  def update
  	@developer = Developer.find params[:id]
  	if @developer.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@developer = Developer.new
  end

  def create  	
  	if @developer = Developer.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:developer).permit!  	
 end  
end

