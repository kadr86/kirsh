class SelectObjectsController < ApplicationController
 before_filter :autentificate, except: :index

  def index
    @id_page = 20
    @select_objects = SelectObject.take
    render layout: 'site'
  end

  def new
    @select_object = SelectObject.new
  end

  def create
    @select_object = SelectObject.new post_params
    if @select_object.save
      redirect_to admin_path
    else
      render :new
    end
  end


  def edit
    @select_object = SelectObject.find params[:id]
  end

  def update
    @select_object = SelectObject.find params[:id]
    if @select_object.update post_params
      redirect_to admin_path
    else
      render :edit
    end

  end

  private

  def post_params
    params.require(:select_object).permit!
  end

end
