# encoding: utf-8

class SessionsController < ApplicationController
	skip_before_filter :autentificate, :only => [:new, :create]
  layout 'site'

  def new
  	@title = "Войти"
  end

  def create

  	@user = UserAccount.authenticate(params[:sessions][:login],params[:sessions][:password])
  	if @user.nil?  		
      flash[:error] = "Вы ввели не верные данные"
  		redirect_to signin_path
  	else
  	 sign_in @user
  	 redirect_back_or admin_path
  	end	
  end

  def destroy
  	sign_out
    redirect_to root_path
  end

end
