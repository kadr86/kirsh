# encoding: utf-8

class ClientRequestWithoutObjectsController < ApplicationController

  before_filter only: [:index, :edit, :new, :load_request, :create, :update] do 
    @personals = {}    
    Personal.where(status: 1).order(:sename).each { |persona| @personals["#{persona.sename} #{persona.name}"] = persona.id}
  end  

  before_filter only: [:new, :create, :edit, :update] do    
    @regions = {}    
    Region.all.each {|region| @regions[region.name] = region.id}
  end

  def index
    @dates =[]
   dates_from_db = ClientRequestWithoutObject.select(:created_at).group(:created_at)
   dates_from_db.each {|date| @dates << date[:created_at].strftime('%m.%Y')}
   @dates.uniq!
  
   if params[:date].nil?        
    last = @dates.last
    last = last.split('.')
    year_last = last[1].to_i
    month_last = last[0].to_i
    end_day = Date.civil(year_last, month_last, -1).day   
    @client_requests = ClientRequestWithoutObject.where(created_at: ["#{year_last}-#{month_last}-01".."#{year_last}-#{month_last}-#{end_day}"])
   else 
    param = params[:date].split('.')
    year = param[1].to_i
    month = param[0].to_i
    end_day = Date.civil(year, month, -1).day   
    @client_requests = ClientRequestWithoutObject.where(created_at: ["#{year}-#{month}-01".."#{year}-#{month}-#{end_day}"])
   end 
   
   @count = @client_requests.count

  	# @client_requests = ClientRequestWithoutObject.all
   #  @count = @client_requests.count     
  end

  def new  	
    @client_request = ClientRequestWithoutObject.new
  end

  def create  	
    @client_request = ClientRequestWithoutObject.new(post_params)
    if @client_request.save
      redirect_to client_request_without_objects_path
    else
      render 'new'
    end
  end

  def edit
  	 @client_request = ClientRequestWithoutObject.find params[:id] 
  end

  def update
  	 @client_request = ClientRequestWithoutObject.find params[:id] 
    if @client_request.update(post_params)
      redirect_to client_request_without_objects_path, notice: "Проспект отредактирован"
    else
      render 'edit'
    end
  end

  def destroy
  	if ClientRequestWithoutObject.find(params[:id]).destroy
      redirect_to client_request_without_objects_path
    else
      redirect_to client_request_without_objects_path
    end
  end

  def load_request   
   from = params[:filter][:from]
   to = params[:filter][:to]
   type = params[:filter][:type]
   rooms_count = params[:filter][:rooms_count]   
   rieltor = params[:filter][:rieltor]
   status = params[:filter][:status]
   query = {}
   query[:created_at] = [from..to] unless from.blank? && to.blank?
   query[:structure_type] = type unless type.blank?   
   query[:rooms_count] = rooms_count unless rooms_count.blank?   
   query[:personal_id] = rieltor unless rieltor.blank? 
   query[:status] = status unless status.blank?

   @client_requests = ClientRequestWithoutObject.where(query)
   @count = @client_requests.count 
   render 'index'
  end

  private

  def post_params
   params.require(:client_request_without_object).permit!
  end   

end