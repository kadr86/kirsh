class FranchasingsController < ApplicationController
  def edit
  	@franchasing = Franchasing.take
  end

  def update
  	@franchasing = Franchasing.find params[:id]
  	if @franchasing.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@franchasing = Franchasing.new
  end

  def create  	
  	if @franchasing = Franchasing.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:franchasing).permit!  	
 end  
end

