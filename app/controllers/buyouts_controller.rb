class BuyoutsController < ApplicationController
  def edit
  	@buyout = Buyout.take
  end

  def update
  	@buyout = Buyout.find params[:id]
  	if @buyout.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@buyout = Buyout.new
  end

  def create  	
  	if @buyout = Buyout.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:buyout).permit!  	
 end  
end
