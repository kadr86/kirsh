class KreditsController < ApplicationController
 
  def edit
  	@kredit = Kredit.find params[:id]
  end

  def update
  	@kredit = Kredit.find params[:id]
  	if @kredit.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@kredit = Kredit.new
  end

  def create  	
  	if @kredit = Kredit.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:kredit).permit!  	
 end  

end


