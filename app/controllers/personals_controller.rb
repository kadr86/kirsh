# encoding: utf-8

class PersonalsController < ApplicationController
  def index
    if params[:status].presence
  	 @personals = Personal.where(status: params[:status]).order("sename ASC") unless params[:status].to_i == 0
     @personals = Personal.all.order("sename ASC") if params[:status].to_i == 0
    else 
     @personals = Personal.where(status: 1).order("sename ASC")
   end
  end

  def new
  	@personal = Personal.new
  end

  def create
  	@personal = Personal.new(post_params)
    if @personal.save  
  		redirect_to personals_path, notice: "Сотрудник добавлен"
  	else
  		render 'new'
  	end
  end

  def edit
    @personal = Personal.find params[:id]
  end

  def update
    @personal = Personal.find params[:id]
    if @personal.update(post_params)
      redirect_to personals_path, notice: "Сотрудник отредактирован"
    else
      render 'edit'
    end
  end

  def show
  end

  def destroy
    @personal = Personal.find params[:id]
    if @personal.destroy
      redirect_to personals_path, notice: "Сотрудник удален"
    else
      redirect_to personals_path, notice: "Сотрудник не был удален"
    end
  end

private

  def post_params
  	params.require(:personal).permit!  	
  end

end
