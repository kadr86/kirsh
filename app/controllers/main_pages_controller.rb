class MainPagesController < ApplicationController
  def edit
  	@main = MainPage.take
  end

  def update
  	@main = MainPage.find params[:id]
  	if @main.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@main = MainPage.new
  end

  def create  	
  	if @main = MainPage.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:main_page).permit!  	
 end  
end
