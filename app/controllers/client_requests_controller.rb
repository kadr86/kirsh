# encoding: utf-8
class ClientRequestsController < ApplicationController
  include SaleObjectsHelper

  before_filter only: [:index, :edit, :new, :load_request] do 
    @personals = {}    
    Personal.where(status: 1).order("sename ASC").each { |persona| @personals["#{persona.sename} #{persona.name}"] = persona.id}
  end  

  before_filter only: [:new, :edit] do
    
    @regions = {}
    @districts = []
    @objects = SaleObject.where(status:[3])
    Region.all.each {|region| @regions[region.name] = region.id}
    if Region.exists? 1
      region = Region.find 1
      if region.districts.any?
       region.districts.order("name ASC").each do |district|
        @districts << district.name
       end
      end
    end
  end
  


  def index
   @dates =[]
   dates_from_db = ClientRequest.select(:created_at).group(:created_at)
   dates_from_db.each {|date| @dates << date[:created_at].strftime('%m.%Y')}
   @dates.uniq!
  
   if params[:date].nil?  	    
    last = @dates.last
    last = last.split('.')
    year_last = last[1].to_i
    month_last = last[0].to_i
    end_day = Date.civil(year_last, month_last, -1).day   
    @client_requests = ClientRequest.where(created_at: ["#{year_last}-#{month_last}-01".."#{year_last}-#{month_last}-#{end_day}"])
   else 
    param = params[:date].split('.')
    year = param[1].to_i
    month = param[0].to_i
    end_day = Date.civil(year, month, -1).day   
    @client_requests = ClientRequest.where(created_at: ["#{year}-#{month}-01".."#{year}-#{month}-#{end_day}"])
   end 
   # render text: ["#{year}-#{month}-01".."#{year}-#{month}-#{end_day}"]
   @count = @client_requests.count
   
  end

  def new
    @client_request = ClientRequest.new  
    @client_request.build_client
  end

  def create
    @client_request = ClientRequest.new(post_params)      
    if @client_request.save()     
      redirect_to client_requests_path, notice: "Проспект добавлен"
    else
      render 'new'
    end
  end

  def edit
    @client_request = ClientRequest.find params[:id] 
  end

  def update
    @client_request = ClientRequest.find params[:id] 
    if @client_request.update(post_params)
      redirect_to client_requests_path, notice: "Проспект отредактирован"
    else
      render 'edit'
    end
  end

  def show
  end

  def load_request
   
   from = params[:filter][:from]
   to = params[:filter][:to]
   type = params[:filter][:type]
   rooms_count = params[:filter][:rooms_count]
   status = params[:filter][:status]
   rieltor = params[:filter][:rieltor]
   query = {}
   query[:created_at] = [from..to] unless from.blank? && to.blank?
   query[:structure_type] = type unless type.blank?
   query[:status] = status unless status.blank?
   query[:rooms_count] = rooms_count unless rooms_count.blank?   
   query[:personal_id] = rieltor unless rieltor.blank? 

   @client_requests = ClientRequest.includes(:sale_object).where(query)
   @count = @client_requests.count 
   render 'index'
  end

  def check_phone
    if Client.where(phone: params[:phone]).take
      render text: 1
    else
      render text: 0
    end
  end

  def destroy
    if ClientRequest.find(params[:id]).destroy
      redirect_to client_requests_path, notice: "Проспект удален"
    else
      redirect_to client_requests_path, notice: "Проспект не удален"
    end
  end

private
  
  def post_params
    params.require(:client_request).permit!    
  end    
end
