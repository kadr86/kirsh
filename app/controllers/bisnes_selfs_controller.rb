class BisnesSelfsController < ApplicationController
  def edit
  	@bisnes_self = BisnesSelf.take
  end

  def update
  	@bisnes_self = BisnesSelf.find params[:id]
  	if @bisnes_self.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@bisnes_self = BisnesSelf.new
  end

  def create  	
  	if @bisnes_self = BisnesSelf.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:bisnes_self).permit!  	
 end  
end

