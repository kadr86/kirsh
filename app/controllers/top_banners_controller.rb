# encoding: utf-8

class TopBannersController < ApplicationController
  def index
  	@top_banners = TopBanner.all
  end

  def new
  	@top_banner = TopBanner.new
  end

  def create
  	@top_banner = TopBanner.new(post_params)
  	if @top_banner.save
  		redirect_to top_banners_path
  	else
  		render 'new'
  	end
  end

  def edit
  	@top_banner = TopBanner.find params[:id]  	
  end

  def update
  	@top_banner = TopBanner.find params[:id]  	
  	if @top_banner.update(post_params)
  		redirect_to top_banners_path
  	else
  		render 'edit'
  	end
  end

  def show
  	@top_banner = TopBanner.find params[:id]
  end

  def destroy
    if TopBanner.find(params[:id]).destroy
      redirect_to top_banners_path
    else
      redirect_to top_banners_path, notice: "Не удалось удалить баннер"
    end
  end

private
	def post_params
		params.require(:top_banner).permit!
	end
end
