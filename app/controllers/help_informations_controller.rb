class HelpInformationsController < ApplicationController
  before_filter :autentificate, except: :index
  def index
    @id_page = 21
    @help_informations = HelpInformation.take
    render layout: 'site'
  end

  def new
    @help_information = HelpInformation.new
  end

  def create
    @help_information = HelpInformation.new post_params
    if @help_information.save
      redirect_to admin_path
    else
      render :new
    end
  end

  def edit
    @help_information = HelpInformation.find params[:id]
  end

  def update
    @help_information = HelpInformation.find params[:id]
    if @help_information.update post_params
      redirect_to admin_path
    else
      render :edit
    end
  end

  private

  def post_params
    params.require(:help_information).permit!
  end
end
