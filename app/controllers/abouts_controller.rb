class AboutsController < ApplicationController

	def new
	@about = About.new	
	end


	def create
		if @about= About.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
	end

	def edit
		@about = About.take
	end

	def update
		@about = About.find(params[:id])
  	if @about.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
	end

private
 def params_hash
 	params.require(:about).permit! 	
 end
end
