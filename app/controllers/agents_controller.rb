class AgentsController < ApplicationController
  def edit
  	@agent = Agent.find params[:id]
  end

  def update
  	@agent = Agent.find params[:id]
  	if @agent.update(params_hash)
  		redirect_to admin_path
  	else
  		render 'edit'
  	end
  end

  def new
  	@agent = Agent.new
  end

  def create  	
  	if @agent = Agent.create(params_hash)
  		redirect_to admin_path
  	else
  		render 'new'
  	end
  end



private
 def params_hash
 	params.require(:agent).permit!  	
 end  
end

