class AnswersController < ApplicationController
  def index
  	@questions = Question.all
  end

  def edit
  	@answer = Answer.find params[:id]
  end

  def new
  	@answer = Answer.new
  	@question = Question.find params[:id]
  end

  def create
  	if @answer = Answer.create(params_hash)
  		QuestionMailer.new_answer(@answer.question.email).deliver unless @answer.question.email.nil?
      q = Question.find @answer.question.id
      q.update(is_public: 1)
  		redirect_to answers_path
  	else
  		render 'new'
  	end
  end

  def update
  	@answer = Answer.find params[:id]
  	if @answer.update(params_hash)
  		redirect_to answers_path(type: 2)
  	else
  		render 'edit'
  	end
  end

  def destroy
  	Question.find(params[:id]).destroy
  	redirect_to answers_path
  end

private

	def params_hash
		params.require(:answer).permit!
	end
  
end
