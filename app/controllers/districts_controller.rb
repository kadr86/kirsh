class DistrictsController < ApplicationController
  def new
  	@region = Region.find params[:region_id]
  	@district = District.new
  end

  def create
  	if @district = District.create(post_params)
  		redirect_to new_district_path(region_id: @district.region_id)
  	else
  		render 'new'
  	end
  end

  def edit
  	@district = District.find params[:id]
  end

  def update
  	@district = District.find params[:id]
  	if @district.update(post_params)
  		redirect_to new_district_path(region_id: @district.region_id)
  	else
  		render 'edit'
  	end
  end


  private
   def post_params
   	params.require(:district).permit!
   end
end
