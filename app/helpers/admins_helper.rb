module AdminsHelper

	def getBirthdays
	personals = Personal.where(status: 1).where.not(birthday: nil)
	text = []
	personals.each do |birthday|
		if birthday.birthday.month == Time.current.month
			text << "<b>#{Russian::strftime(birthday.birthday, '%d %B')}:</b> #{birthday.sename} #{birthday.name}".html_safe if birthday.birthday.day >= Time.current.day && birthday.birthday.day <= (Time.current+1.weeks).day 
		end
	end
	text
end

end
