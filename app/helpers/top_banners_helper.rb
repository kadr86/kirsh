# encoding: utf-8
module TopBannersHelper

  def all_pages_url
   {
   	 "О компании" => "/sites/about",
   	 "Вопрос Ответ" => "/sites/question_answer",
   	 "Контакты" => "/sites/contacts",
   	 "Займы под залог недвижимости" => "/sites/zalog",
   	 "Срочный выкуп недвижимости" => "/sites/buyout",
   	 "Кредитным организациям" => "/sites/kredit",
   	 "Агенствам недвижимости" => "/sites/agents",
   	 "Собственникам коммерческой недвижимости" => "/sites/owner_komerch",
   	 "Застройщикам" => "/sites/developer",
   	 "Услуги" => "/sites/search",
   	 "Оставить заявку" => "/sites/request_user",
   	 "Заявка на размещение объявления" => "/sites/request_advert"
   }
	end
end
