# encoding: utf-8

module NewsOnSitesHelper

def public_but
	image_tag 'public.png', title: "Опубликованно"
end

def unpublic_but
	image_tag 'unpublic.png', title: "Не опубликованно"
end

def delete
	image_tag 'cross.png', title: "Удалить", width: 16
end

end
