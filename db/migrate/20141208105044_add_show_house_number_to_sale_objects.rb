class AddShowHouseNumberToSaleObjects < ActiveRecord::Migration
  def change
    add_column :sale_objects, :show_number, :integer
  end
end
