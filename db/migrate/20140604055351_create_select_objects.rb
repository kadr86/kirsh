class CreateSelectObjects < ActiveRecord::Migration
  def change
    create_table :select_objects do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
