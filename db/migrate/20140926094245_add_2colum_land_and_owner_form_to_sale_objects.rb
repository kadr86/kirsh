class Add2columLandAndOwnerFormToSaleObjects < ActiveRecord::Migration
  def change
    add_column :sale_objects, :owner_form, :integer
    add_column :sale_objects, :land, :string
  end
end
