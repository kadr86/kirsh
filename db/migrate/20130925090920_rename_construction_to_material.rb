class RenameConstructionToMaterial < ActiveRecord::Migration
  def change
  	rename_column :client_requests, :construction, :material
  end
end
