class RenameContructionToMaterialToClientRequestWithoutObject < ActiveRecord::Migration
  def change
  	rename_column :client_request_without_objects, :construction, :material
  end
end
