class ChangeStatusTypeToIntToPersonals < ActiveRecord::Migration
  def change
  	change_column :personals, :status, :integer
  end
end
