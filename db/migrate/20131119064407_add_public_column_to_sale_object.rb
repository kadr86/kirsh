class AddPublicColumnToSaleObject < ActiveRecord::Migration
  def change
    add_column :sale_objects, :public, :integer
  end
end
