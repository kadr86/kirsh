class CreateTopBanners < ActiveRecord::Migration
  def change
    create_table :top_banners do |t|
      t.string :page_url
      t.string :image_url

      t.timestamps
    end
  end
end
