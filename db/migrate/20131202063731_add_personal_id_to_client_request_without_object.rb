class AddPersonalIdToClientRequestWithoutObject < ActiveRecord::Migration
  def change
    add_column :client_request_without_objects, :personal_id, :integer
  end
end
