class AddLocalityTypeToClientRequestWithObject < ActiveRecord::Migration
  def change
    add_column :client_request_without_objects, :locality_type, :string
  end
end
