class DereteAddPosition < ActiveRecord::Migration
  def change
  	remove_column :personals, :position
  	add_column :personals, :position, :integer
  end
end
