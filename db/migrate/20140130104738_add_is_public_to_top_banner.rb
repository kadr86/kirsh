class AddIsPublicToTopBanner < ActiveRecord::Migration
  def change
    add_column :top_banners, :is_public, :integer
  end
end
