class ChangePasswordNameAndAddSaltColumnToUserAccounts < ActiveRecord::Migration
  def change
  	change_table :user_accounts do |t|
  		t.rename :password, :encrypted_password
  		t.string :salt
  	end
  end

  
end
