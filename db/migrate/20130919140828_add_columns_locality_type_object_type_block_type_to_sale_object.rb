class AddColumnsLocalityTypeObjectTypeBlockTypeToSaleObject < ActiveRecord::Migration
  def change
  	add_column :sale_objects, :locality_type, :string
  	add_column :sale_objects, :object_type, :string
  	add_column :sale_objects, :block_type, :string
  end
end
