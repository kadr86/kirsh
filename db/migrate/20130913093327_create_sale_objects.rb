class CreateSaleObjects < ActiveRecord::Migration
  def change
    create_table :sale_objects do |t|
      t.references :personal, index: true
      t.integer :sold, default: 0
      t.integer :expired, default: 0
      t.integer :avans, default: 0
      t.string :region, null: false
      t.string :locality, null: false
      t.string :area, null: false
      t.string :district, null: false
      t.string :street_type, null: false
      t.string :street, null: false
      t.string :object, null: false
      t.string :letter
      t.string :block
      t.integer :flat
      t.string :index
      t.string :station_metro
      t.string :agency, null: false
      t.integer :status, null: false
      t.date :begin_contract, null: false
      t.date :end_contract
      t.string :owner
      t.string :owner_phone
      t.float :price, null: false
      t.integer :hide_price, null: false, default: 0
      t.string :client_request
      t.string :client_phone
      t.float :finaly_price
      t.date :date
      t.string :structure_type, null: false
      t.integer :rooms_count
      t.float :general_square
      t.float :live_square
      t.float :kittchen_square
      t.float :area_square
      t.string :sanysel
      t.string :material
      t.integer :floor
      t.integer :all_floors
      t.integer :balkon, null: false, default: 0
      t.integer :lodjiya, null: false, default: 0
      t.integer :parking, null: false, default: 0
      t.integer :garage, null: false, default: 0
      t.integer :lift, null: false, default: 0
      t.integer :phone_line, null: false, default: 0
      t.string :nswe
      t.text :description
      t.integer :agreement, null: false, default: 0

      t.timestamps
    end
  end
end
