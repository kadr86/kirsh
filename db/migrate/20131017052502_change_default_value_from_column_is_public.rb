class ChangeDefaultValueFromColumnIsPublic < ActiveRecord::Migration
  def change
  	change_column :news_on_sites, :is_public, :integer, default: 1
  end
end
