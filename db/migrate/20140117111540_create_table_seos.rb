class CreateTableSeos < ActiveRecord::Migration
  def change
    create_table :seos do |t|
    	t.string :page_name
    	t.string :title
    	t.string :description
    	t.string :keywords
    end
  end
end
