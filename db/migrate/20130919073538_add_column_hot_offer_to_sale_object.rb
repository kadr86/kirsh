class AddColumnHotOfferToSaleObject < ActiveRecord::Migration
  def change
  	add_column :sale_objects ,:hot_offer, :integer, default: 0 
  end
end
