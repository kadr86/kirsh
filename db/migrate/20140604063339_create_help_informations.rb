class CreateHelpInformations < ActiveRecord::Migration
  def change
    create_table :help_informations do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
