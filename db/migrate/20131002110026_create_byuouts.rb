class CreateByuouts < ActiveRecord::Migration
  def change
    create_table :byuouts do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end
end
