class RenameColumInEffectiveRieltors < ActiveRecord::Migration
  def change
  	rename_column :effectiv_rieltors, :active_sold, :sale_object_id
  	add_index :effectiv_rieltors, :sale_object_id 
  	remove_column :effectiv_rieltors, :active_arenda
  	remove_column :effectiv_rieltors, :avans
  	remove_column :effectiv_rieltors, :sold
  	remove_column :effectiv_rieltors, :put
  	remove_column :effectiv_rieltors, :expired
  end
end
