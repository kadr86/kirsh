class AddNewColumnBuildingSquareToSaleObjects < ActiveRecord::Migration
  def change
    add_column :sale_objects, :building_square, :real
  end
end
